
from kfp import dsl
import mlrun
# import configargparse
from mlrun import new_function, auto_mount, code_to_function, import_function

# Create a Kubeflow Pipelines pipeline. Give a name and description
@dsl.pipeline(
    name="Case5 Pipeline",
    description="Demonstrate Case5"
)

#the actual pipeline is run here
def kfpipeline(container):
    
    project = mlrun.get_current_project()
    print(f'container = {container}')
    
#     # ================ Step 4: Deploy Stream ===========================
    print("starting deploy stream step")
    serving_fn = project.get_function("deploy_trigger")
    print("get function done")
    serving_fn = serving_fn.apply(auto_mount())
    graph = serving_fn.set_topology("flow")
    graph.to(name="serving-trigger", handler='model_serving_1').respond()
    print("before deploy")
    serving_deployed = mlrun.deploy_function(serving_fn)
